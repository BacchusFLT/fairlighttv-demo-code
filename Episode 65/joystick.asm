// A joystick controlled sprite
// Bacchus of FairLight
// 2023-01-19

.const JoyPort = $dc00

BasicUpstart2(start)

start:	sei

	lda #$7f
	sta $dc0d 	// Turn off CIA#1
	sta $dd0d 	// Turn off CIA#2

	lda #$01 
	sta $d01a 	// Turn on Raster interrupt
	asl $d019 	// Clear pending IRQ

	lda #$f1	// Set the IRQ to row $f1
	sta $d012
	lda $d011 	// Clear the MSB of raster
	and #$7f 
	sta $d011

	lda #<IRQ
	sta $0314 
	lda #>IRQ 
	sta $0315 

	// Setup sprites

	ldx #$00 	//Copy sprite data
!:	lda sprite,x 
	sta $0340,x 
	inx 
	cpx #$3f 
	bne !-	

	// Sprite slots are every $40 bytes
	// Bank is 0 so no need to compensate for that

	lda #$0340/$40 	// Set sprite pointer
	sta $07f8 	

	lda #$01 	// Enable sprite
	sta $d015

	lda #$70 	// Just a position on the visible screen
	sta $d000
	sta $d001

	// Should normally populate all values and not rely on default

	cli
	rts
	//jmp *-3

IRQ:	inc $d020

	lda $d001 	// A little something to set colour
	lsr 	// Based on the Y position
	lsr
	lsr
	sta $d027 	// Flash sprite

	lda JoyPort
	and #$01
	bne !+
	dec $d001 	//Joy_Up

!:	lda JoyPort
	and #$02
	bne !+ 
	inc $d001 	// Joy_Down

!:	lda JoyPort
	and #$04
	bne !+
	dec $d000

!:	lda JoyPort
	and #$08
	bne !+
	inc $d000

!:	lda JoyPort
	and #$10
	bne !+

	lda $d01d 
	eor #$01 	// Toggle lowest bit
	sta $d01d 	// Store back to expand X
	sta $d017 	// Store to Expand Y
!:

ExitIRQ:	asl $d019
	dec $d020

	jmp $ea31
	//jmp $ea81

// The definition of a sprite

sprite:	.byte %11111110,%11100000,%11111110
	.byte %11111110,%11100000,%11111110
	.byte %11100000,%11100000,%00111000
	.byte %11100000,%11100000,%00111000
	.byte %11100000,%11100000,%00111000
	.byte %11100000,%11100000,%00111000
	.byte %11111000,%11100000,%00111000
	.byte %11111000,%11100000,%00111000
	.byte %11100000,%11100000,%00111000
	.byte %11100000,%11100000,%00111000 // 10
	.byte %11100000,%11100000,%00111000
	.byte %11100000,%11100000,%00111000
	.byte %11100000,%11100000,%00111000
	.byte %11100000,%11100000,%00111000
	.byte %11100000,%11111110,%00111000
	.byte %11100000,%11111110,%00111000
	.byte %00000000,%00000000,%00000000
	.byte %00000000,%00000000,%00000000
	.byte %00000000,%00000000,%00000000
	.byte %00000000,%00000000,%00000000 //20
	.byte %00000000,%00000000,%00000000
