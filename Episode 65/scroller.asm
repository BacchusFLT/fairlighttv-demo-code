// A simple char scroller
// By Bacchus of FairLight
// 2023-01-19

.const 	therow = $07c0

BasicUpstart2(start)

start:	sei

	lda #$7f
	sta $dc0d 	// Turn off CIA#1
	sta $dd0d 	// Turn off CIA#2

	lda #$01 
	sta $d01a 	// Turn on Raster interrupt
	asl $d019 	// Clear pending IRQ

	lda #$f1	// Set the IRQ to row $f1
	sta $d012
	lda $d011 	// Clear the MSB of raster
	and #$7f 
	sta $d011

	lda #<IRQ
	sta $0314 
	lda #>IRQ 
	sta $0315 

	jsr setscroller

	lda #$0e
	jsr $ffd2
	lda #$08
	jsr $ffd2

	cli
	rts
	//jmp *-3

IRQ:	inc $d020

	lda $d016
	and #%11110000 	// Mask fine scroll and 38 column mode
	ora finescroll
	sta $d016

	// The delay loop is long enough to end as the end of the screen
	// Normally you could do productive stuff rather that delays
	// Playing the music is a good thing to do here, rather then the delay

	ldx #$7f
!:	nop
	dex 
	bne !-

	lda #$c8
	sta $d016

	dec finescroll
	bpl nocourse

	lda #$07
	sta finescroll
	jsr doscroll


nocourse:	asl $d019
	dec $d020

	jmp $ea31 	// Go back to Kernal IRQ service routine
	//jmp $ea81

doscroll:	ldy #$00 	// Move the characters on the screen
!:	lda therow+1,y 
	sta therow,y
	iny
	cpy #$27 
	bne !-

!:	ldy #$00 
	lda ($fe),y 
	bne !+ 	// The char is not the last so go to store it

	jsr setscroller 
	jmp !-

!:	sta therow+$27 	// Last position of the screen

	inc $fe 
	lda $fe 
	bne !+
	inc $ff
!:	rts

setscroller:	lda #<scrolltext
	sta $fe 
	lda #>scrolltext
	sta $ff
	rts

.encoding "screencode_mixed"

scrolltext:
	.text "This is a scroller that will show "
	.text "how the raster IRQs work ... "
	.byte 0

finescroll: .byte 8
