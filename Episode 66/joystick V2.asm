// A joystick controlled sprite
// Bacchus of FairLight
// 2023-01-19

// Shows:
// * Movement restrictions
// * Open border
// * Print of position
// * Show the ghost byte
// * Ghost sprite

.const JoyPort = $dc00
.const PrintLocation = $07c0

BasicUpstart2(start)

start:	sei

	lda #$7f
	sta $dc0d 	// Turn off CIA#1
	sta $dd0d 	// Turn off CIA#2

	lda #$01 
	sta $d01a 	// Turn on Raster interrupt
	asl $d019 	// Clear pending IRQ

	lda #$fa	// Set the IRQ to row $f1
	sta $d012
	lda $d011 	// Clear the MSB of raster
	and #$7f 
	sta $d011

	lda #<IRQ
	sta $0314 
	lda #>IRQ 
	sta $0315 

	// Setup sprites

	ldx #$00 	//Copy sprite data
!:	lda sprite,x 
	sta $0340,x 
	inx 
	cpx #$3f 
	bne !-	

	// Sprite slots are every $40 bytes
	// Bank is 0 so no need to compensate for that

	lda #$0340/$40 	// Set sprite pointer
	sta $07f8 	

	lda #$01 	// Enable sprite
	sta $d015

	lda #$70 	// Just a position on the visible screen
	sta $d000
	sta $d001

	// Should normally populate all values and not rely on default

	lda #$93
	jsr $ffd2

	cli
	rts
	//jmp *-3

IRQ:	inc $d020

	lda #$13 	// 24 rows - to open border
	sta $d011

	lda $d001 	// A little something to set colour
	lsr 	// Based on the Y position
	lsr
	lsr
	sta $d027 	// Flash sprite

	// Do UP
	lda JoyPort
	and #$01
	bne No_Up
	dec $d001 	//Joy_Up
No_Up:

	// Do DOWN
	lda JoyPort
	and #$02
	bne No_Down
	inc $d001 	// Joy_Down
No_Down:	

	// Do LEFT
	lda JoyPort
	and #$04
	bne No_Left

	lda $d010
	and #$01
	bne !+
	lda $d000
	//cmp #$00
	beq No_Left

!:	dec $d000
	lda $d000
	cmp #$ff 
	bne No_Left
	lda $d010 	// $100 -> $FF
	and #$fe 
	sta $d010
No_Left:

	// Do RIGHT
	lda JoyPort
	and #$08
	bne No_Right

	lda $d010
	and #$01
	beq !+
	lda $d000
	cmp #$58
	beq No_Right

!:	inc $d000
	lda $d000
	bne No_Right
	lda $d010 	//FF->100
	ora #$01
	sta $d010
No_Right:

	// Do FIRE
	lda JoyPort
	and #$10
	bne No_Button

	lda $d01d 
	eor #$01 	// Toggle lowest bit
	sta $d01d 	// Store back to expand X
	sta $d017 	// Store to Expand Y
No_Button:

	// Print the position

	lda #'$'
	sta PrintLocation
	sta PrintLocation+4

	lda $d000 	// X position
	ldy #$01
	jsr DoPrint

	lda $d001 	// Y position
	ldy #$05
	jsr DoPrint

	// Done printing

	lda #$1b  	// Reset $d011
	sta $d011

	lda $d000  	// Show the ghost byte
	sta $3fff

	dec $d020

	asl $d019
	jmp $ea31
	//jmp $ea81

// The definition of a sprite

sprite:	.byte %11111110,%11100000,%11111110
	.byte %11111110,%11100000,%11111110
	.byte %11100000,%11100000,%00111000
	.byte %11100000,%11100000,%00111000
	.byte %11100000,%11100000,%00111000
	.byte %11100000,%11100000,%00111000
	.byte %11111000,%11100000,%00111000
	.byte %11111000,%11100000,%00111000
	.byte %11100000,%11100000,%00111000
	.byte %11100000,%11100000,%00111000 // 10
	.byte %11100000,%11100000,%00111000
	.byte %11100000,%11100000,%00111000
	.byte %11100000,%11100000,%00111000
	.byte %11100000,%11100000,%00111000
	.byte %11100000,%11111110,%00111000
	.byte %11100000,%11111110,%00111000
	.byte %00000000,%00000000,%00000000
	.byte %00000000,%00000000,%00000000
	.byte %00000000,%00000000,%00000000
	.byte %00000000,%00000000,%00000000 //20
	.byte %00000000,%00000000,%00000000

// ======================================
// Print hex byte
// Call with value in A and offset in Y
// ======================================

DoPrint:	pha
	iny
	and #%00001111
	jsr prcount2
	pla
	dey
	lsr
	lsr
	lsr
	lsr
prcount2:	tax
	lda printset,x
prcount3:	sta PrintLocation,y   //Absolut adress!
	rts

	.encoding "screencode_upper" 

printset:	.text "0123456789ABCDEF"
