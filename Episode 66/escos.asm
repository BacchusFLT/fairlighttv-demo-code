/* 
   ESCOS - By 1001 crew
   Uses 4000 5fff as screens
   Uses 6000 6dff for sprite data

   8 * 7 expanded sprites
   Sprites are hence 48 * 42 pixels

   Original code by 1001 crew (Honey or TSI?)
   Adaptation by Bacchus/Fairlight

   Tut picture from the 1001 release

*/

.var thecount = $ff

	*=$c000

	// Set up the spritepointers in the respective screens

	ldy #$00
	sty thecount
!:	clc
	lda thecount
	adc #$80
	sta $43f8,y 	// Sprite on Row 04
	lda thecount
	adc #$88
	sta $47f8,y 	// 2E
	lda thecount
	adc #$90
	sta $4bf8,y 	// 58
	lda thecount
	adc #$98
	sta $4ff8,y 	// 82
	lda thecount
	adc #$a0
	sta $53f8,y 	// AC
	lda thecount
	adc #$a8
	sta $57f8,y 	// D6
	lda thecount
	adc #$b0
	sta $5bf8,y 	// 00

	inc thecount

	iny
	cpy #$08
	bne !-

  	sei
	lda #$7f
	sta $dc0d
	sta $dd0d

	lda #>IRQ1
	sta $0315
	lda #<IRQ1
	sta $0314

	ldy #$2e
!:  	lda VICDefaults,y
	sta $d000,y
	dey
	bpl !-

	lda #$96 	// VIC bank at $4000
	sta $dd00

	asl $d019
	cli

	lda #$1b
	sta thecount

  	jmp *-3

// The values of $d018 which points to the different possible screens

D018Table:	.byte $07,$17,$27,$37,$47,$57,$67
	
VICDefaults:	.byte $f0,$04    //D000
	.byte $28,$04
	.byte $58,$04
	.byte $88,$04
	.byte $b8,$04
	.byte $e8,$04
	.byte $18,$04
	.byte $48,$04
	.byte %11000001   //D010
	.byte $1b         //D011
	.byte $00 	  //D012
	.byte $d1
	.byte $00
	.byte $ff
	.byte $c8        //D016
	.byte $ff
	.byte $17        //D018
	.byte $00
	.byte $01        //D01a
	.byte $00
	.byte $ff
	.byte $ff
	.byte $ff
	.byte $ff
	.byte $f0        //D020
	.byte $f0
	.byte $f1
	.byte $f2
	.byte $f3
	.byte $09 	//D025 - Sprite col 1
	.byte $07 	//D026 - Sprite col 2
	.fill 8,$08 	//D027 - Individual sprite colour

// Delay package - Set of lebels that yields a fixed delay

Delay36:	nop
Delay34:	nop
Delay32:	nop
Delay30:	nop
Delay28:	nop
Delay26:	nop
Delay24:	nop
Delay22:	nop
Delay20:	nop
Delay18:	nop
Delay16:	nop
Delay14:	nop
Delay12:	rts

// ============================
// First raster interrupt
// ============================

IRQ1:	lda #$02 	//Set next IRQ to line 2
	sta $d012
	asl $d019
	lda #<IRQ2  	//Added
	sta $0314 	//Added
	lda #>IRQ2
	sta $0315
	cli
	nop
	nop
	nop
	nop
	nop
	nop
	nop
	nop
	nop
	nop
	nop
	nop
	nop
	nop
	nop
	nop
	nop
	nop
	nop
	nop
	nop
	nop
	nop
	nop
	nop
	nop
	nop
	nop
	nop
	nop
	nop
	nop
	nop
	nop
	nop
	nop
	nop
	nop
	nop
	nop
	nop
	nop
	nop
	nop
	nop
	nop
	nop
	nop
	nop
	nop
	nop
	nop
	nop
	nop
	nop
	nop
	nop
	nop
	nop
	nop
	nop
	nop
	nop
	nop
	nop
	nop
	nop
	nop
	nop
	nop
	nop
	nop
	nop
	nop
	nop
	nop
	nop
	nop
	nop
	nop
	nop
	nop
	nop
	nop
	nop
	nop
	nop
	nop
	nop
	nop
	nop
	nop
	nop
	nop
	nop
	nop
	nop
	nop
	nop
	nop
	nop
	nop
	nop
	nop
	nop
	nop

	// IRQ2 never happens -> reset

	jmp     $fce2

// ============================
// First raster interrupt
// ============================
// Sprites are on Row $4 when we get here - Set 0
// X points to the $d018 

IRQ2:	jsr Delay18
	bit $ea 	// Delay 3
	lda $d012
	cmp #$02
	beq Adjust     //Stable raster
Adjust:  
	jsr Delay36 	// Delay 72
	jsr Delay36

	ldx #$01
	jsr Paint40Lines

	lda #$2e 	// Set the sprites to Row $2e - Set 1
	jsr SetSpriteY 	// and set $d018

	jsr Delay26

	dec $d016
	inc $d016
	jsr Paint38Lines

	lda #$58 	// Set the sprites to Row $58 - Set 2
	jsr SetSpriteY 	// and set $d018

	jsr Delay28

	dec $d016
	inc $d016
	jsr Paint38Lines

	lda #$18
	sta $d011
	lda #$82 	//Set the sprites to Row $82 - Set 3
	sta $d001
	sta $d003
	sta $d005
	sta $d007
	sta $d009
	dec $d016 	// Note that it does $d016 here
	inc $d016
	sta $d00b
	sta $d00d
	sta $d00f
	lda D018Table,x
	sta $d018
	inx

	jsr Delay12

	dec $d016
	inc $d016
	jsr Paint39Lines

	lda #$ac 	// Set the sprites to Row $ac - Set 4
	jsr SetSpriteY 	// and set $d018

	jsr Delay22

	lda #$1b
	sta $d011 	// Y is zero
	dec $d016
	inc $d016
	jsr Paint38Lines

	lda #$d6 	// Set the sprites to Row $d6 - Set 5
	jsr SetSpriteY 	// and set $d018

	jsr Delay26
	ldy #$00 	// Prepare the Y

	dec $d016
	inc $d016
	jsr Paint31Lines
	lda #$10 	// Upper lower opening?
	sta $d011,y 	// ",Y" to add one cycle

	jsr Delay18
	bit $24 	// Delay 3

	dec $d016
	inc $d016

	jsr Delay34

	dec $d016
	inc $d016

	jsr Delay34

	dec $d016
	inc $d016
	jsr Paint3Lines

	lda #$00 	// Set the sprites to Row $00 - Set 6
	jsr SetSpriteY 	// and set $d018

	jsr Delay28

	dec $d016
	inc $d016
	jsr Paint40Lines

	// Prepare for next frame

	lda #$04
	sta $d001
	sta $d003
	sta $d005
	sta $d007
	sta $d009
	sta $d00b
	sta $d00d
	sta $d00f

	lda #$07
	sta $d018
	lda #$1b
	sta $d011
	lda #$00 	// IRQ1 on line 0
	sta $d012
	lda #<IRQ1
	sta $0314
	lda #>IRQ1
	sta $0315

	asl $d019

	pla 	//Pull the first interrupt
	pla
	pla
	plp
	pla
	pla

	jmp $ea81

	// End of IRQ2

SetSpriteY:	sta $d001
	sta $d003
	sta $d005
	sta $d007
	sta $d009
	dec $d016 	// Border opening here
	inc $d016
	sta $d00b
	sta $d00d
	sta $d00f
	lda D018Table,x
	sta $d018
	inx 	// Increas the screen pointer

	jsr Delay12
	dec $d016
	inc $d016
	rts

HandleALine:	lda $d012
	and #$07
	cmp #$02 	// Compare with normal scroll
	bne HAL3
	lda #$18
	sta $d011
HAL2:	nop
	nop
	nop
	dec $d016
	inc $d016
	rts

HAL3:  	cmp #$04
	bne HAL2

	lda thecount        // Needs to be indirect
	sta $d011
	dec $d016
	inc $d016
	rts

// Set of routines that handles a fixed number of rasterlines

Paint40Lines:	jsr HandleALine
Paint39Lines:	jsr HandleALine // 39 lines
Paint38Lines:	jsr HandleALine // 38 lines
	jsr HandleALine
	jsr HandleALine
	jsr HandleALine
	jsr HandleALine
	jsr HandleALine
	jsr HandleALine
Paint31Lines:  	jsr HandleALine
	jsr HandleALine // 30 lines
	jsr HandleALine
	jsr HandleALine
	jsr HandleALine
	jsr HandleALine
	jsr HandleALine
	jsr HandleALine
	jsr HandleALine
	jsr HandleALine
	jsr HandleALine
	jsr HandleALine // 20 lines
	jsr HandleALine
	jsr HandleALine
	jsr HandleALine
	jsr HandleALine
	jsr HandleALine
	jsr HandleALine
	jsr HandleALine
	jsr HandleALine
	jsr HandleALine
	jsr HandleALine // 10 lines
	jsr HandleALine
	jsr HandleALine
	jsr HandleALine
	jsr HandleALine
	jsr HandleALine
	jsr HandleALine
Paint3Lines:  	jsr HandleALine // 3 lines
	jsr HandleALine
	jsr HandleALine
	bit $ea
	jmp HandleALine


	*=$6000

	.import c64 "tut.pic"
