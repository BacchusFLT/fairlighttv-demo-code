/************************************************************

 Microload Transfer
 (c) 2023 - Bacchus of FairLight

 Including work of Fungus of NOS,
 Inspired by Agile (K12 and Syncro)

 Made for 
 * Velociped (by Karl Hörnell. With Micropainter)

Tested and confirmed working for  
* Auriga (With Micropainter)
* Back to the Future (Loading picture) - Works after relocation is added
* Bandana City (Custom Loadingscreen)
* Bigtop Barney (Custom Loadingscreen)
* Break Feaver (Custom Loadingscreen)
* Caverns Of Sillahc (Custom Loadingscreen)
* Chameleon (No load entertainment)
* China Miner (No load entertainment)
* Cleanup Time (by Karl Hörnell. With Micropainter)
* Colossus Chess 4 (Loading picture)
* Dessert Hawk (With Micropainter)
* Dandy (Loading picture) - Works after relocation is added
* Elektrix (With Micropainter)
* Front line (Custom Loadingscreen)
* Fruity (by Karl Hörnell. With Micropainter)
* Fungus (by Karl Hörnell. With Micropainter)
* Jumpman (Loading picture - why does it have two sides?)
* Mission Elevator (Loading picture)
* Mermaid Madness (No load entertainment)
* Nuclear Embargo (Loading picture) - Works after relocation is added
* Ronald Rubberduck (by Karl Hörnell. With Micropainter)
* Steve Davis Snooker (Custom Loadingscreen)
* Tales of the Arabian Night (Loading picture)- Works after relocation is added
* The Prodigy (Loading picture)
* Velocipede II (by Karl Hörnell. With Micropainter)
* Wild Ride (With Micro rescue)

Partly working for 
* Big Ben (Automatic $FA doesn't work)
* Exploding fist by Micropool (Loading picture, automatic $FA doesn't work)
* Hits Volume 1 ($FA doesn't work. You need to wind around to fetch the relevant header)
* Movie Monster Game (Automatic $FA doesn't work. Not really tested)
* Pitstop II (Loading picture. Automatic $FA doesn't work)
* Vapire's empire (Loading picture, automatic $FA doesn't work. Needs relocation)
* Winter Games (Automatic $FA doesn't work)

Not working
* All games working

V0.3: 
Smart $FA handling - extract value from the loader, rather dumb hardcoded value
More handling of filename - skip more characters that are not wanted
Prints the first jump as well

V0.4
Added relocation

V0.5
Added RunStop to override smart $FA

V0.6
Changed buffer to $0800 and now The Prodigy works

V0.7
Changed the leading character to A rather than 0, for games with multiple files

V0.8
Open screen

V0.9
Strip end of filename spaces.

V0.10
Implemented "Complete" message

To do:
* There is a bit of an issue that saves stop in some bad cases. 

*************************************************************/

// The place on screen where the messages are posted
.const statusbox   = $07c0
.const loaderstart = $0478

// The place in the string where the start address is posted
.const startaddyplot = msg_startaddy-msg_found
.const startaddy = $03f5
.const firststartplot = msg_firststart-msg_found
.const firststart = $12f0 	// $02f0 but relocated
.const fileoffset = 5 	// Offset for the Filneme to be plotted

.const bufferstart = $0800

// Zeropage usage
.var cd      	= $02
.var counter 	= $03  // Relevant during save
.var temp 	= $05
// temp+0/1  - Start address
// temp+2/3  - Length
// temp+4/5  - End

	* = $0801

	.word basicend
	.word 2023                      // Line number
	.byte $9e                       // SYS
	.text  "2061"
basicend:       .byte $00,$00,$00

.print  toIntString(Start) + " must be 2061"


// This section is the generalk init

Start:          sei
	ldx #$ff
	txs

	lda $ba
	sta cd       //save current drive

	lda #$0b     //setup
	sta $d011
	lda #$c1     //no nmi
	sta $0318
	lda #$80
	sta $0291    //no char shift
	lda #$17
	sta $d018    //this set
	lda #$00
	sta $9d      //no msgs
	sta $d020
	sta $d021
	sta $0286    //black

	lda #$2f     //i/o config
	sta $00      //bit 4 = input
	lda $01
	and #$06     //mem config
	ora #$20     //motor off
	sta $01

	lda #$93
	jsr $ffd2    //clr screen

	lda #$1b     //screen back on
	sta $d011

	jsr PrintMain

	ldx #$00     //print main msg
!:	lda mainmsg,x
	sta $0400,x
	lda #$05
	sta $d800,x
	inx
	cpx #$78     // 3 lines
	bne !-


	jsr populateloader

	ldy #msg_pressplay-messages
	jsr printstatus

	lda $01 	// Await play press
!:	cmp $01
	beq !-

// From here we load from tape and analyse the header and first file

	cli
	jsr loadhead 	// Basically SYS 63276 without the print

	ldy #msg_found-messages
	jsr printstatus


// Load also the $02AF file
// Changes the load address for the small file so that the $FA value can be extracted

	lda #$12
	sta $033e
	lda #$13
	sta $0340
	jsr 62828       // $f56c

// This parses the filename and skips control characters

	ldy #05
	ldx #fileoffset 	// Offset to print the filename
!:	lda ($b2),y
	cmp #$20 	// Skip characters with PETSCII under $30
	bcc skipchar
	cmp #$60 
	bcs skipchar
	cmp #$2a 	// Skip *
	beq skipchar

okchar:	sta statusbox,x
	sta filename-fileoffset+2,x  //X is 7 and it should be on offset 2
	lda #$01
	sta statusbox+$d400,x
	inx
skipchar:	iny
	cpy #$15
	bne !-

// Establish the filename length

	ldx #$10
!:	lda filename,x 
	cmp #$20
	bne !+
	dex	
	bne !-
!:	inx
	stx fnamlen+1

// Plot the address to the init of the first loaded file

	lda firststart
	sta counter
	lda firststart+1
	sta counter+1

	ldy #firststartplot
	jsr printaddress

// Plot the start address

	lda startaddy
	sta counter
	lda startaddy+1
	sta counter+1

	ldy #startaddyplot 	// Plot the start address of the game
	jsr printaddress

// Fetch the $FA value(=numberof files) from the loader

	ldx $12df
	lda $12e1
	cmp #$fa   	// If this value is not $FA, then the read value is not the parameter
	bne !+
	txa
!:	.byte $2c
	lda #$FF 	// Then just read as many files as possible
	sta $fa

	jsr Keywait	// Await keypress so people can note start address
	cmp #$7f  	// Pressing Run/Stop loads $FF files
	bne !+ 	// Override the smart $FA
	lda #$ff
	sta $fa

!:	jmp tapestart 	// From here, all RAM from $0800 is available - the entire routine is on screen

//===========================
// Load the header from tape
//===========================

loadhead:	jsr tapeon
	lda #$01     //open tape file
	tax
	ldy #$00
	jsr $ffba
	lda #$00
	tax
	tay
	jsr $ffbd
	lda #$00
	sta $90
	sta $93
	jsr $f7d7    //load header/file
	jsr $f84f
	jmp tapeoff     //motor off

// ======================================
// Copy the loader to the screen memory
// ======================================

populateloader:
	ldx #$00     //kill input
	stx $0289
!:	lda loader,x
	sta loaderstart,x
	lda loader+$0100,x
	sta loaderstart+$0100,x
	lda loader+$0200,x
	sta loaderstart+$200,x
	lda loader+$0240,x
	sta loaderstart+$240,x
	inx
	bne !-
	rts

// ======================================
// The loader built to be run on $0478
// ======================================

loader:
.pseudopc $0478
{

tapestart: 	jsr tapeio
Nextfile:      	ldy #msg_file-messages
	jsr printstatus

	jsr tapeon
	jsr AwaitFA
FinalSave:	sei
	jsr tapeoff
	jsr save
	lda $fa
	bne Nextfile

	ldy #msg_done-messages
	jsr printstatus

	inc $d020 	// This is where the program ends
	jmp *-3 	// Endless loop of border flash

// Utility: Await for the loader to flag that it's loaded a new segment
// It decreases $FA when loading is done

AwaitFA:	cli
	lda $fa
!:	cmp $fa
	beq !-
	rts

// Init the tap loader - set up the IRQ

tapeio:	sei
	lda     #$7f
	sta     $dc0d
	sta     $dd0d
	ldy     #$00
	sty     $d01a
	sty     $dd0e
	lda     #$4d
	sta     $dd04
	lda     #$01
	sta     $dd05
	lda     #$90
	sta     $dc0d
	lda     #<IRQ
	sta     $fffe
	lda     #>IRQ
	sta     $ffff
	lda     #$05 	// Memory configuration: Only IO (No Basic nor Kernal)
	sta     $01

	lda     #$b1
	sta     $fb 	// Not sure what this is

!:   	nop  		//Delay
	dex
	bne     !-
	dey
	bne     !-
	iny
	rts

IRQ:     	//dec     $dbe9   - General place for the graphics routine
	pha
	lda $ab  	// Own flash effect
	sta $d020
	lda #$91
	sta $dd0e
	lda $dd0d
	lsr
	lda $dc0d
	ror $bd
	lda $bd
	dey
L0366:   	beq Store02
	pla
	rti

Store02: 	cmp     #$a0
	bne     L0382
	lda     #$0d       //Branch t0 0375
	sta     L0366+1
	bne     ExitNMI

Store0D: 	cmp     #$a0
	beq     ExitNMI
	cmp     #$0a 	//Wait for pilot
	beq     L0385
L037D:   	lda     #$02       //Branch to 036a
	sta     L0366+1
L0382:   	iny
	pla
	rti

L0385:   	lda     #$29
	sta     L0366+1
	lda     #$09       //Compare with 09
	sta     Store29+1  //Branch to 0391
	bne     ExitNMI

Store29: 	cmp     #$00
	bne     L037D
	dec     Store29+1  //Decrease compare
	bne     ExitNMI
	lda     #$40
	sta     L0366+1    //Branch to 03a8
	lda     #$fc       //Store address
	sta     Store40+1
ExitNMI: 	ldy     #$08
	pla
	rti

//*******************************************************************************
//* This part loads the parameter bytes and stores in $FC to $FF (so four       *
//* bytes)                                                                      *
//*                                                                             *
//* $FC/$FD is start and $FE/$FF is $FFFF - length                              *
//*******************************************************************************
Store40: 	sta $00 	//store address - starts at $FC, set above
	inc Store40+1 	//Next address
	bne ExitNMI
	lda #$52
	sta L0366+1 	//Branch to 03BA
	sty $ab 	// Store AB
	jmp storevectors
	nop 	//Padding

Store52: 	sta     ($fc),y
	eor     $ab
	sta     $ab  	// Store AB
	inc     $fc
	bne     _L03C8
	inc     $fd
	dec     $fb
_L03C8:  	inc     $fe
	bne     ExitNMI2
	inc     $ff
	bne     ExitNMI2
	lda     #$71       //Branch to 03d9
	sta     L0366+1

ExitNMI2:      	ldy     #$08
	pla
	rti

Store71: 	eor     $ab        //Check if load is ok
	beq     _L03E4

//	lda     #$37       //Load failed
//	sta     $01
//	jmp     $fcf2

	dec $fa
//	bne L037D
	nop
	nop
	jmp FinalSave

_L03E4:	
	lda     #$01       //Load was OK
	sta     $dbe9
	dec     $fa
	bne     L037D

	jmp FinalSave

storevectors:  	lda $fc 	// Store the originbal load addresses
	sta temp
	lda $fd
	sta temp+1

	lda #<bufferstart 	// Relocate loading
	sta $fc 
	lda #>bufferstart
	sta $fd

	sec  	// Calculate the number of bytes to load
	lda #$00
	sbc $fe
	sta temp+2
	lda #$00 
	sbc $ff 
	sta temp+3

	clc  	// Add to start to tell the end address
	lda temp
	adc temp+2
	sta temp+4

	lda temp+1
	adc temp+3
	sta temp+5

	jmp ExitNMI2


// Provide Y before calling

printaddress:	lda counter+1
	jsr !+
	lda counter
!:	pha
	lsr 
	lsr 
	lsr 
	lsr 
	jsr !+
	pla
!:	and #$0f
	tax
	lda hextab,x
	sta statusbox,y
	lda #$01
	sta statusbox+$d400,y
	iny
	rts

hextab: 	.text "0123456789ABCDEF"


//==================================
// Await space press 
// Flash border

Keywait:	inc $d020
	lda $dc01
	cmp #$ff
	beq Keywait
	rts

// Print to the statusbox
// Y points to offset of the relevant message

printstatus:	ldx #$0
!:              lda messages,y 
	sta statusbox,x
	lda #$0d
	sta statusbox+$d400,x 
	inx
	iny 
	cpx #$28
	bne !-
	rts

messages:
msg_pressplay:  .text "   Command: Press play to find header   "
msg_found:	.text "File:               Init:$"
msg_firststart:	.text "xxxx Run:$"
msg_startaddy:	.text "xxxx"
msg_space2save: .text "   Command: Space to save file to disk  "
msg_file:	.text "Searching and Loading until tape's end  "
msg_saving:	.text "Saving: "
filename: 	.text "A.                  $xxxx-$xxxx "
msg_done:	.text "> Transfer complete - my work is done! <"


// The general save routine

save:        	lda #$36
        	sta $01

        	ldy #msg_saving-messages
	jsr printstatus

	lda temp
	sta counter 	
	lda temp+1
	sta counter+1
	ldy #$1d
	jsr printaddress

	lda temp+4
	sta counter 	
	lda temp+5
	sta counter+1
	ldy #$23
	jsr printaddress

	lda #<bufferstart
	sta counter    //init save addy
	lda #>bufferstart
	sta counter+1

	lda #$01    //open file for
	ldx cd      //write
	ldy #$01
	jsr $ffba   //setlfs
fnamlen:	lda #$10
	ldx #<filename
	ldy #>filename
	jsr $ffbd   //setname
	jsr $ffc0   //open
	ldx #$01
	jsr $ffc9   //chkout
	lda temp     //start addy low
	jsr $ffa8   //send
	lda temp+1     //start addy high
	jsr $ffa8   //send

	ldy #$00    //save the file
saveb:	sei
	lda (counter),y
	jsr $ffa8

	inc counter     
	bne nohigh
	inc counter+1
	inc $d020

nohigh:	sec 
	lda temp+2
	sbc #$01
	sta temp+2
	lda temp+3
	sbc #$00
	sta temp+3

	lda temp+2
	ora temp+3
	bne saveb

nohigh2:	lda #$01
	jsr $ffc3   //close the file

	// Check for error
	lda #$00
	jsr $ffbd
	lda #$0f
	tay
	ldx cd
	jsr $ffba
	jsr $ffc0
	lda cd
	jsr $ffb4   //talk
	lda #$6f
	jsr $ff96   //tlksa
	jsr $ffa5
	cmp #$30
	beq noerr

	jsr Keywait
	jmp save 	// If we had an error, signal and enforce andother save

noerr:	jsr $ffa5
	cmp #$0d
	bne noerr
	jsr $ffab   //untlk

	lda #$0f
	jsr $ffc3
	inc filename
	lda #$35
	sta $01
	rts

tapeon:	lda $01
	and #$d7
	sta $01
	rts

tapeoff:	sei
	lda $01
	ora #$20
	sta $01
	rts

loaderend:

	// This pads all that is copied with spaces. Also assembly with fail if
	// negative, so it also cathes a possible error

	.fill loaderstart+$0340-loaderend,$20

}
mainmsg:             //1234567890123456789012345678901234567890
	.text "========================================"
	.text "  FairLightTV Microload Transfer v0.10  "
	.text "========================================"


PrintMain:	ldx #$00
!:	lda openingscreen,x 
	sta $0400,x 
	lda openingscreen+$100,x 
	sta $0400+$100,x 
	lda openingscreen+$200,x 
	sta $0400+$200,x 
	lda openingscreen+$2e8,x 
	sta $0400+$2e8,x 
	lda #$01
	sta $d800,x 
	sta $d900,x 
	sta $da00,x 
	sta $dae8,x
	inx 
	bne !-

!:	jsr $ffe4
	beq !-

	lda #$93
	jmp $ffd2

openingscreen:
                     //1234567890123456789012345678901234567890
row1:	.text "Microload transfer - (c) FairLight 2023 "
row2:	.text "                                        "
row3:	.text "Transfers most know games using this    "
row4:	.text "loader type.                            "
row5:	.text "                                        "
row6:	.text "Usage:                                  "
row7:	.text "Insert tape, run program and press play "
row8:	.text "Once the header is found, press *space* "
row9:	.text "to transfer all files using automated   "
row10:	.text "detection of number of files.           "
row11:	.text "Make note of the start addresses printed"
row12:	.text "                                        "
row13:	.text "Press *Run/Stop* to override the        "
row14:	.text "automated detection of number of files. "
row15:	.text "                                        "
row16:	.text "The rest is automatic if you have enough"
row17:	.text "space on the destination disk.          "
row18:	.text "                                        "
row19:	.text "Disk errors will prompt saving again -  "
row20:	.text "just insert a new disk and press space  "
row21:	.text "to continue the process.                "
row22:	.text "                                        "
row23:	.text "Happy hacking!                  /Bacchus"
row24:	.text "                                        "
row25:	.text "                (SPACE)                 "


