// Graphics priority anomaly
// Bacchus of FairLight
// 2025-03-02

.const JoyPort = $dc00
.const PrintLocation = $07c0

BasicUpstart2(start)

start:	sei

	lda #$7f
	sta $dc0d 	// Turn off CIA#1
	sta $dd0d 	// Turn off CIA#2

	lda #$01 
	sta $d01a 	// Turn on Raster interrupt
	asl $d019 	// Clear pending IRQ

	lda #$fa	// Set the IRQ to row $f1
	sta $d012
	lda $d011 	// Clear the MSB of raster
	and #$7f 
	sta $d011

	lda #<IRQ
	sta $0314 
	lda #>IRQ 
	sta $0315 

	// Setup sprites

	ldx #$00 	//Copy sprite data
!:	lda Sprite,x 
	sta $0340,x 
	inx 
	cpx #$3f 
	bne !-	

	// Sprite slots are every $40 bytes
	// Bank is 0 so no need to compensate for that

	lda #$0340/$40 	// Set sprite pointer
	sta $07f8 
	sta $07f9

	lda #$03 	// Enable sprites
	sta $d015

	lda #$60 	// Just a position on the visible screen
	sta $d001
	sta $d003

	lda #$40
	sta $d000
	lda #$60
	sta $d002

	lda #$01 	// Priority
	sta $d01b

	// Should normally populate all values and not rely on default

	lda #<StartText
	ldy #>StartText
	jsr $ab1e

	lda #$00 
	sta $3fff

	cli
	rts

CurrentSpr:	.byte 0


IRQ:	ldx CurrentSpr 	// Set the raster colour to the current sprite
	inx
	stx $d020

	lda #$13 	// 24 rows - to open border
	sta $d011

	lda CurrentSpr 
	asl 
	tax

	// Do UP
	lda JoyPort
	and #$01
	bne No_Up
	dec $d001,x 	//Joy_Up
No_Up:
	// Do DOWN
	lda JoyPort
	and #$02
	bne No_Down
	inc $d001,x  	// Joy_Down
No_Down:	
	// Do LEFT
	lda JoyPort
	and #$04
	bne No_Left

	lda $d010
	and DoMask,x
	bne !+
	lda $d000,x 
	beq No_Left

!:	dec $d000,x 
	lda $d000,x 
	cmp #$ff 
	bne No_Left
	lda $d010 	// $100 -> $FF
	and UnDoMask,x
	sta $d010
No_Left:
	// Do RIGHT
	lda JoyPort
	and #$08
	bne No_Right

	lda $d010
	and DoMask,x
	beq !+
	lda $d000,x 
	cmp #$58
	beq No_Right

!:	inc $d000,x 
	lda $d000,x 
	bne No_Right
	lda $d010 	//FF->100
	ora DoMask,x
	sta $d010
No_Right:
	lda JoyPort
	and #$10
	bne NoButton

	lda CurrentSpr 	// Toggle current sprite 
	eor #$01
	sta CurrentSpr

NoButton:
	// Print the position

	lda #'$'
	sta PrintLocation
	sta PrintLocation+4

	sta PrintLocation+8
	sta PrintLocation+12

	lda $d000 	// X position
	ldy #1
	jsr DoPrint

	lda $d001 	// Y position
	ldy #5
	jsr DoPrint

	lda $d002	// X position
	ldy #9
	jsr DoPrint

	lda $d003 	// Y position
	ldy #13
	jsr DoPrint


	// Done printing

	lda #$1b  	// Reset $d011
	sta $d011

	lda #14
	sta $d020

	asl $d019
	jmp $ea31 	// End IRQ and also with keyscan

DoMask:	.byte $01,$02
UnDoMask:	.byte $ff,$fe

// ======================================
// Print hex byte
// Call with value in A and offset in Y
// ======================================

DoPrint:	pha
	iny
	and #%00001111
	jsr prcount2
	pla
	dey
	lsr
	lsr
	lsr
	lsr
prcount2:	tax
	lda printset,x
prcount3:	sta PrintLocation,y
	rts

	.encoding "screencode_upper" 

printset:	.text "0123456789ABCDEF"

// ======================================
// Sprite
// ======================================


Sprite: 	.byte %00001111,%11111111,%11110000
	.byte %00111111,%11111111,%11111100
	.byte %01111111,%11111111,%11111110
	.fill 15*3,$ff
	.byte %01111111,%11111111,%11111110
	.byte %00111111,%11111111,%11111100
 	.byte %00001111,%11111111,%11110000

// ======================================
// Starting text
// ======================================

	.encoding "petscii_mixed" 

StartText:	.byte $93,$9b,$0e
	.text "Sprite Priority"
	.byte $0d,$0d 
	.text "Sprite 0 (W) - OVER R - UNDER chars"
	.byte $0d
	.text "Sprite 1 (R) - UNDER W - OVER chars"
	.byte 0